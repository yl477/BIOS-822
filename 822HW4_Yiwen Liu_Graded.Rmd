---
title: "822HW4_Yiwen Liu"
output:
  pdf_document: default
  html_document:
    df_print: paged
---
_**100 points**_  

Page 123: Problems 1-4  
1. How can you tell if an object is a tibble? (Hint: try printing mtcars, which is a regular data frame.)  
```{r}
mtcars
```
```{r}
library(tidyverse)
as_tibble(mtcars)
```
```{r}
# mtcars is a dataframe and the class is dataframe
class(mtcars)
# class of a tibble will produce more output
class(as_tibble(mtcars))
```

A tibble returns the first 10 rows and all the columns that fit on screen. Instead, a dataframe returns all rows and columns in the dataframe.  

2. Compare and contrast the following operations on a data.frame and equivalent tibble. What is different? Why might the default data frame behaviors cause you frustration?  
```{r}
df <- data.frame(abc = 1, xyz = "a")
df$x
df[, "xyz"]
df[, c("abc", "xyz")]
```
```{r}
df2 <- tibble(abc = 1, xyz = "a")
df2$x
df2[, "xyz"]
df2[, c("abc", "xyz")]
```
Dataframes allow partial matching and calling $x will return a vector with the levels and contents of variable "xyz" in the dataframe. In contrast tibbles don't allow for partial matching and will show an error message (Unknown or uninitialised column: 'x'.NULL).  
Calling [, "xyz"] on a dataframe returns a vector with the contents of the variable (levels, etc). In contrast calling [, "xyz"] on a tibble returns contents of variable "xyz" in the tibble format.  
Calling [, c("abc", "xyz")] on the dataframe and the tibble returns the same content.  
The default dataframe behavior with $x causes frustration because it allows partial matching and sometimes will match to unwanted variables.  

3. If you have the name of a variable stored in an object, e.g., var <- "mpg", how can you extract the reference variable from a tibble?  
```{r}
var <- "mpg"  
as_tibble(mtcars) %>% select(var)
as_tibble(mtcars)[, var]
```
Either piping to select or using [] will allow you to extract the reference variable of var.  

4. Practice referring to nonsyntactic names in the following data frame by:  
```{r}
annoying <- tibble(
`1` = 1:10,
`2` = `1` * 2 + rnorm(length(`1`))
)
```
a. Extracting the variable called 1.  
```{r}
annoying %>% select(`1`)
```

b. Plotting a scatterplot of 1 versus 2.  
```{r}
# plot(data=annoying, x=`1`,y=`2`)
g1 <- ggplot(data=annoying, aes(x=`1`,y=`2`))
g1 + geom_point()
```

c. Creating a new column called 3, which is 2 divided by 1.  
```{r}
annoying %>% mutate(`3` = `2` / `1`) -> annoying
annoying
```

d. Renaming the columns to one, two, and three:  
```{r}
annoying <- rename(annoying, one=`1`, two=`2`, three=`3`)
annoying
```

Page 129: Problem 4  
4. Sometimes strings in a CSV file contain commas. To prevent them from causing problems they need to be surrounded by a quoting character, like " or '. By convention, read_csv() assumes that the quoting character will be ", and if you want to change it you’ll need to use read_delim() instead. What arguments do you need to specify to read the following text into a data frame?  
```{r}
read_delim("x,y\n1,'a,b'", delim=',', quote="\'", escape_backslash = T)
```

Page 156: Problems 1-4  
1. Why are gather() and spread() not perfectly symmetrical?
Carefully consider the following example:  
```{r}
stocks <- tibble(
year = c(2015, 2015, 2016, 2016),
half = c( 1, 2, 1, 2),
return = c(1.88, 0.59, 0.92, 0.17)
)

stocks

stocks %>%
spread(year, return) %>%
gather("year", "return", `2015`:`2016`)
```
(Hint: look at the variable types and think about column names.)
Both spread() and gather() have a convert argument. What does it do?  
In the original stocks tibble, the "year" variable is stored as a double. In contrast, after transformation using spread() and gather(), the "year" variable is now a character.This is because variable names are always converted to a character vector by gather().   
The convert argument will coerce the value column into a string when it contains a mix of variable types. However in this case the "year" variable is in fact a factor, and is coerced to character before type conversion if convert=T, which returns the "year" variable as integer in the transformed tibble.  
```{r}
stocks %>%
spread(year, return) %>%
gather("year", "return", `2015`:`2016`, convert=T)
```
2. Why does this code fail?  
```{r error=TRUE}
table4a %>%
gather(1999, 2000, key = "year", value = "cases")
```
This code fails because 1999 and 2000 are special/non-standard variable names, and should be included in ticks (``) for reference. Without ticks, R is trying to look for 1999th and 2000th columns in the tibble.     
```{r}
table4a %>%
gather(`1999`, `2000`, key = "year", value = "cases")
```

3. Why does spreading this tibble fail? How could you add a new column to fix the problem?  
```{r error=TRUE}
people <- tribble(
~name, ~key, ~value,
#-----------------|--------|------
"Phillip Woods", "age", 45,
"Phillip Woods", "height", 186,
"Phillip Woods", "age", 50,
"Jessica Cordero", "age", 37,
"Jessica Cordero", "height", 156
)

people %>% spread(key,value)
```
Spreading the tibble fails because there are two observations in age for Phillip Woods.  
One way to fix the problem is to add another column to account for the observation number for each person.  
```{r}
people2 <- tribble(
~name, ~obs_num, ~key, ~value,
#-----------------|--------|------
"Phillip Woods",1, "age", 45,
"Phillip Woods", 1, "height", 186,
"Phillip Woods", 2, "age", 50,
"Jessica Cordero", 1, "age", 37,
"Jessica Cordero", 1, "height", 156
)

people2 %>% spread(key,value)
```

4. Tidy this simple tibble. Do you need to spread or gather it? What are the variables?  
```{r}
preg <- tribble(
~pregnant, ~male, ~female,
"yes", NA, 10,
"no", 20, 12
)

preg %>% gather(key=Gender, value=count, male, female) %>% mutate(pregant=ifelse(pregnant == "yes", "Yes", "No"), Gender_female = ifelse(Gender == "female", "Yes", "No")) %>% select(-Gender)
```
We need to gather it and the variables are pregnant, count, pregnant and gender_female.  

Page 186: Problems 1 and 2.  
1. Compute the average delay by destination, then join on the airports data frame so you can show the spatial distribution of delays. Here’s an easy way to draw a map of the United States:
```{r}
library(nycflights13)
library(maps)
airports %>%
semi_join(flights, c("faa" = "dest")) %>%
ggplot(aes(lon, lat)) +
borders("state") + geom_point() +
coord_quickmap()
```
(Don’t worry if you don’t understand what semi_join() does — you’ll learn about it next.)
You might want to use the size or color of the points to display the average delay for each airport.
```{r}
flights %>% select(arr_delay,dest) %>% group_by(dest) %>% summarize(avg_delay = mean(arr_delay, na.rm=T)) -> dest_grp
dest_grp
```
```{r}
dest_grp %>%
inner_join(airports, by=c("dest" = "faa")) %>%
ggplot(aes(lon, lat,color = avg_delay)) +
borders("state") + geom_point() +
coord_quickmap()
```
2. Filter flights to only show flights with planes that have flown at least 100 flights.
```{r}
flights %>% select(flight, tailnum) %>% group_by(tailnum) %>% summarise(count=n()) %>% filter(count > 100) -> hundred_flights
flights %>% semi_join(hundred_flights,by="tailnum")
```

