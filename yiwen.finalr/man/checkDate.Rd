% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/checkDate.R
\name{checkDate}
\alias{checkDate}
\title{checkDate
Check if a given date is in the historical cherry blossom festival range in DC}
\usage{
checkDate(input)
}
\arguments{
\item{input}{a string in the DATE format}
}
\value{
return an output saying whether the input date is in the range of the festival or no info
}
\description{
A function named checkDate that takes in a string in DATE form and returns an output depending on whether the date is in the historical cherry blossom festival range in DC.
}
